export const cartItemCount = (state) => {
      return state.cart.length
}
export const cartTotalPrice = (state)=>{
     let total =0;
    state.cart.forEach(element => { 
       if(element){   
        total +=element.variant.price*element.quantity
       }
    });
     return total;
}  
export const productData = (state)=>{
      if(state){
      return state.collection[0] && state.collection[0].products
      }
 }  
 
 export const productDetails = (state)=>{
      if(state){
      return state.product
      }
 }  