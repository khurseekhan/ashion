export const GET_PRODUCT_LIST = (state, products) => {
  let title = products.products.map((c) => c.title);
  var filterList = products.products.concat(
    state.collection.filter((item) => !title.includes(item.title))
  );
  if (state.collection.length <= 0) {
    state.collection.push(products);
  } else {
    state.collection = [];
    state.collection.push(products);
  }
};
export const ADD_TO_CART = (state, product) => {
  let title = product.map((c) => c.title);
  var filterList = product.concat(
    state.cart.filter((item) => !title.includes(item.title))
  );
  if (filterList) {
    state.cart = [];
    state.cart.push(...filterList);
  } else {
    state.cart.push(...product);
  }
};
export const REMOVE_PRODUCT_FROM_CART = (state, product) => {
  if (product) {
    state.cart = [];
    state.cart.push(...product);
  }
};

export const UPDATE_CART_ITEMS = (state, product) => {
  if (product) {
    state.cart = [];
    state.cart.push(...product);
  }
};
export const ADD_DISCOUNT = (state, product) => {
  if (product) {
    state.cart = [];
    state.cart.push(...product);
  }
};
export const CHECK_OUT = (state, product) => {
  if (product) {
    state.cart = [];
    state.cart.push(...product);
  }
};

export const SORT_LOWER_PRICE = (state, id) => {
  const data = state.collection[id].products;
  data.sort((a, b) => {
    if (a.variants[0].price > b.variants[0].price) return 1;
    if (a.variants[0].price < b.variants[0].price) return -1;
    return 0;
  });
  data.sort((a, b) => {
    return a.variants[0].price - b.variants[0].price;
  });
  state.collection[id] = { ...state.collection[id], products: data };
};
export const SORT_HIGH_PRICE = (state, id) => {
  const data = state.collection[id].products;
  data.sort((a, b) => {
    if (a.variants[0].price > b.variants[0].price) return 1;
    if (a.variants[0].price < b.variants[0].price) return -1;
    return 0;
  });

  data.sort((a, b) => {
    return a.variants[0].price - b.variants[0].price;
  });
  const highPrice = data.reverse();
  state.collection[id] = { ...state.collection[id], products: highPrice };
};
export const LATEST = (state, id) => {
  const data = state.collection[id].products;
  data.sort((a, b) => {
    if (a.createdAt > b.createdAt) return 1;
    if (a.createdAt < b.createdAt) return -1;
    return 0;
  });
  state.collection[id] = { ...state.collection[id], products: data };
};

export const BEST_SELLER = (state, id) => {
  const data = state.collection[id].products;
  data.sort((a, b) => {
    if (a.createdAt > b.createdAt) return 1;
    if (a.createdAt < b.createdAt) return -1;
    return 0;
  });
  state.collection[id] = { ...state.collection[id], products: data };
};


export const PRODUCT_DETAILS = (state, id) => {
  console.log("rawId", id)
  if(state){
  const data = state.collection[0].products.filter(item => {
     return item.id === id
  });
  console.log("action calling3", data)
  state.product =[]
  state.product.push(...data)
  }
 };
