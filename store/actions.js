import Client from "shopify-buy";
import "isomorphic-fetch"
const client = Client.buildClient({
  domain: "vsf-next-pwa.myshopify.com",
  storefrontAccessToken: "03f21475b97c18fa05c0ab452c368af4",
});
export const getProductLists = ({ commit }, product) => {
  client.collection.fetchAllWithProducts().then((collections) => {
  if(collections){
  const collectionId = collections[product.id].id;
  client.collection.fetchWithProducts(collectionId, {productsFirst: product.limit}).then((collection) => {
  commit("GET_PRODUCT_LIST", collection);  
});
}
});
};
export const addToCart = ({ commit }, product) => {
  const checkoutId =
    "Z2lkOi8vc2hvcGlmeS9DaGVja291dC8wNmM0MzMzNGY4ZTg5MmE0NWY0Yzk3YjliZWM3NmIwZT9rZXk9YWI5ZGFlNDEzMGVmYzEwZTAzNWRmYjNjYWZjNDZhOWI=";
  const lineItemsToAdd = [
    {
      variantId: product.variantId,
      quantity: product.qty,
      customAttributes: [{ key: "MyKey", value: "MyValue" }],
    },
  ];
  client.checkout.addLineItems(checkoutId, lineItemsToAdd).then((checkout) => {
    if (checkout.lineItems) {
      commit("ADD_TO_CART", checkout.lineItems);
    }
  });
};

export const removeProductFromCart = ({ commit }, id) => {
  const checkoutId =
    "Z2lkOi8vc2hvcGlmeS9DaGVja291dC8wNmM0MzMzNGY4ZTg5MmE0NWY0Yzk3YjliZWM3NmIwZT9rZXk9YWI5ZGFlNDEzMGVmYzEwZTAzNWRmYjNjYWZjNDZhOWI="; // ID of an existing checkout
  const lineItemIdsToRemove = [id];
  client.checkout
    .removeLineItems(checkoutId, lineItemIdsToRemove)
    .then((checkout) => {
      if (checkout.lineItems) {
        commit("REMOVE_PRODUCT_FROM_CART", checkout.lineItems);
      }
    });
};

export const updateCartItems = ({ commit }, product) => {
  const checkoutId =
    "Z2lkOi8vc2hvcGlmeS9DaGVja291dC8wNmM0MzMzNGY4ZTg5MmE0NWY0Yzk3YjliZWM3NmIwZT9rZXk9YWI5ZGFlNDEzMGVmYzEwZTAzNWRmYjNjYWZjNDZhOWI="; // ID of an existing checkout
  const lineItemsToUpdate = [{ id: product.id, quantity: product.qty }];
  client.checkout
    .updateLineItems(checkoutId, lineItemsToUpdate)
    .then((checkout) => {
      if (checkout.lineItems) {
        commit("UPDATE_CART_ITEMS", checkout.lineItems);
      }
    });
};

export const addDiscount = ({ commit }, product) => {
  const checkoutId =
    "Z2lkOi8vc2hvcGlmeS9DaGVja291dC8wNmM0MzMzNGY4ZTg5MmE0NWY0Yzk3YjliZWM3NmIwZT9rZXk9YWI5ZGFlNDEzMGVmYzEwZTAzNWRmYjNjYWZjNDZhOWI="; // ID of an existing checkout
  const discountCode = product;
  client.checkout.addDiscount(checkoutId, discountCode).then(checkout => {
    if (checkout) {      
      commit("ADD_DISCOUNT", checkout.lineItems);
    }
  });
};
export const checkOut = ({ commit }, product) => {
  const checkoutId = 'Z2lkOi8vc2hvcGlmeS9DaGVja291dC8wNmM0MzMzNGY4ZTg5MmE0NWY0Yzk3YjliZWM3NmIwZT9rZXk9YWI5ZGFlNDEzMGVmYzEwZTAzNWRmYjNjYWZjNDZhOWI='
  const data = client.checkout.fetch(checkoutId).then((checkout) => {
    commit("CHECK_OUT", checkout.lineItems);
    return checkout.webUrl
    });
  return data
};
export const sortLowerPrice = ({ commit }, id) => {
  commit("SORT_LOWER_PRICE", id);
};

export const sortHighPrice = ({ commit }, id) => {
  commit("SORT_HIGH_PRICE", id);
};
export const latest = ({ commit }, id) => {
       commit("LATEST", id);
}
export const bestSaller = ({ commit }, id) => {
  commit("BEST_SELLER", id);
}
export const productDetails = ({ commit },id)=>{
  console.log("action calling", id)
  commit("PRODUCT_DETAILS", id);
 }